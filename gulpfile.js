var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();


gulp.watch('assets/scss/**/*.scss', ['sass']); 


gulp.task('browserSync', function() {
  browserSync.init({
    server: {
      baseDir: 'assets'
    },
  })
})
gulp.task('serve', function() {
  gulp.src('developer-challenge')
    .pipe(webserver({
      port:'3000',
      livereload: true,
      open: true
    }));
});
gulp.task('sass', function() {
  return gulp.src('assets/scss/**/*.scss') // Gets all files ending with .scss in app/scss
    .pipe(sass())
    .pipe(gulp.dest('assets/css'))
    .pipe(browserSync.reload({
      stream: true
    }))
});